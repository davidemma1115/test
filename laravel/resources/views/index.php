<!doctype html>
<html lang="es">
<head>
	<title>CANOPY RIVER</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
	<link rel="shortcut" href="img/logo.jpg" type="image/x-icon">
	<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="js/jquery-2.1.4.js"> </script>
	<script type="text/javascript">

	function color_uno() {
		 $("#visualizador").fadeIn(0);
	  $("#visualizador").html("<img src='img/imagen.jpg'>");
	  
	setTimeout('color_dos()', 3000);
	}

	function color_dos() {
		 $("#visualizador").fadeIn(3000);
	  $("#visualizador").html("<img src='img/imagen2.jpg'>");
	   
	setTimeout('color_tres()', 3000);
	}
	function color_tres() {
		 $("#visualizador").fadeIn(3000);
	  $("#visualizador").html("<img src='img/imagen.jpg'>");
	  
	setTimeout('color_uno()', 3000);
	}

	color_uno()
	</script>
	<script type="text/javascript" src="../js/eskju.jquery.scrollflow.min.js"></script>
</head>
<body>
	<header>
		<div id='img_header'>
			<img src="img/canopy_river.png">
		</div>
		<div id='informacion_header'>
			<img src="img/icon1.png">
			<span>CALL NOW 1 888 334 8877</span>
			<span id='lenguaje'>ESPAÑOL</span>
			<span id='contact'>CONTACT US</span>
			<img src="img/icon2.png">
			<img src="img/icon3.png">
			<img src="img/icon4.png">
			<img src="img/icon5.png">
			<img src="img/icon6.png">

		</div>
		<div id='menu_header'>
			<span id='home'>HOME</span>
			<span id='activities'>ACTIVITIES</span>
			<span id='restaurant'>RESTAURANT</span>
			<span id='gallery'>GALLERY</span>
			<span id='ptov'>PUERTO VALLARTA</span>
			<span id='blog'>BLOG</span>
			<span id='ejido'>EJIDO</span>
			<span class='boton'>BOOK NOW</span>
		</div>
		
	</header>
	<div id='visualizador'>
		<img src="img/imagen.jpg">
	</div>
	<div id='seccion1'>
		<span id='mensaje'>BOOK ONLINE AND SAVE 15% OFF</span>
		<span>DATE:</span>
		<span id='fecha'><?php $hoy = date("d/m/y"); echo $hoy;?></span>
		<span>ACTIVITY:</span>
		<span id='actividad'>ZIP LINE</span>
		<img src="img/AmericaExpress.png">
		<img src="img/Visa.png">
		<img src="img/MasterCard.png">
		<img src="img/PayPal.png">
		<span class='boton'>CONTINUE</span>
	</div>
	<div id='seccion2'>
		<h2>ZIP LINE</h2>
	</div>
	<div id='seccion3'>
		<h3>PREPARE TO EXPERIENCE PURE ADRENALINE</h3>
		<div id='cuadro1' class='scrollflow -slide-right -opacity'>
			<img src="img/duration.png"><br>
			<span>2 Hours(plus shuttle time)</span></div>
		<div id='cuadro2' class='scrollflow -slide-right -opacity'>
			<img src="img/available.png"><br>
			<span>Everyday</span>
			</div>
		<div id='cuadro3' class='scrollflow -slide-left -opacity'>
			<img src="img/capacity.png"><br>
			<span>60 People</span>
			</div>
		<div id='cuadro4' class='scrollflow -slide-left -opacity'>
			<img src="img/activity.png"><br>
			<span>Medium to High</span>
			</div>
	</div>
	<div id='seccion4' >
		<span class='scrollflow -slide-top -opacity'>Feel the wind in you face and butterflies in you stomach as you flay full speed at 200</span>
		<span class='scrollflow -slide-top -opacity'>meter above the river, among the treetops, enjoying the majestic scenery</span>
		<span class='scrollflow -slide-top -opacity'>surrounding CANOPY RIVER, a place where adventure has only one name and the</span>
		<span class='scrollflow -slide-top -opacity'>special guest is you. Ahh! I almost forgot...</span>
		<span class='scrollflow -slide-top -opacity'>WE WILL CLOSE THIS ADVENTURE ABOARD</span>
		<span class='scrollflow -slide-top -opacity'>SOME CUTE AND FLIRTY MULES.</span>
	</div>
	<div id='seccion5'>
		<div  class='circulos scrollflow -pop -opacity'><img src="img/imagen1.png"></div>
		<div  class='circulos scrollflow -pop -opacity'><img src="img/imagen2.png"></div>
		<div  class='circulos scrollflow -pop -opacity'><img src="img/imagen3.png"></div>
		<div  class='circulos scrollflow -pop -opacity'><img src="img/imagen4.png"></div>

	</div>
	<div id='seccion6'>
		<div id='columna1'>
			<h2>WHAT'S INCLUDED</h2>
			<ul class='vinieta1'>
				<li>Shuttle Service</li>
			</ul>
			<ul class='vinieta2'>
			<li>Complementary welcome drink</li>
			</ul>
			<ul class='vinieta3'>
			<li>12 Zip lines</li>
			</ul>
			<ul class='vinieta4'>
			<li>Mule ride(15 min.)</li>
			</ul>
			<ul class='vinieta5'>
			<li>Purified water during excursion</li>
			</ul>
			<ul class='vinieta6'>
			<li>Tequila tasting & tour</li>
			</ul>
		</div>
		<div id='columna2'>
			<h2>WHAT TO WEAR & BRING</h2>
			<ul type=square>
				<li>Light clothing and comfortable shoes</li>
			<li>Insect repellent</li>
			<li>Cameras are not allowed for safety reasons</li>

			</ul>
			
		</div>
		<div id='columna3'>
			<h2>DEPARTS FROM</h2>
			<h4>NUEVO VALLARTA</h4>
			<h4>(Av. M&aecute; #570 Junto a Glorieta del Tigre)</h4>
			<h4>Pick up schedule:</h4>
			<h4>7:50 Hrs. | 8:50 Hrs. | 9:50 Hrs. | 11:50 Hrs. | 13:50 Hrs.</h4>
			<h4> MARINA (Collage Disco)</h4>
			<h4>Pick up schedule:</h4>
			<h4>8:10 Hrs. | 9:10 Hrs.| 10:10 Hrs. | 12:10 Hrs. | 14:10 Hrs.</h4>
			<h4>PLAZA LAS GLORIAS</h4>
			<h4>(Fco. Medina Ascencio #1989 local H2-A)</h4>
			<h4>Pick up schedule:</h4>
			<h4>8:20 Hrs.| 9:20 Hrs.| 10:20 Hrs. | 12:20 Hrs. | 14:20 Hrs. </h4>
			<h4>CENTRO (Insurgentes #379 Col.Emiliano Zapata)</h4>
			<h4>Pick up schedule:</h4>
			<h4>8:40 Hrs. | 9:40 Hrs. | 10:40 Hrs. | 12:40 Hrs | 14:40 Hrs</h4>

		</div>
	</div>
	<div id='seccion7'>
		<img src="img/mapa.png" class='scrollflow -slide-right -opacity'>
<img src="img/tabla_distancia.png" class='scrollflow -slide-left -opacity'>
	</div>

	<div id='seccion8'>
		<h3>RESTRICTIONS</h3>
		<span>Minimum age: 6 years old</span>
		<span>Maximum weight:252 lbs.</span>
		<span>For your own safety, people with</span>
	</div>
	<div id='seccion9'>
		<div id='circulo1'><img src="img/adult.png" class='scrollflow -pop -opacity'><span class='boton'>BOOK NOW</span></div>
		<div id='circulo2'><img src="img/child.png" class='scrollflow -pop -opacity'><span class='boton'>BOOK NOW</span></div>
		<br><h2>FIND MORE WAYS TO EXPERIENCE THE ADVENTURE</h2>
		<div id='cuadros'>
			<div class='cuadros scrollflow -pop -opacity'><img src="img/cuadro1.jpg">
			</div>
			<div class='cuadros scrollflow -pop -opacity'><img src="img/cuadro1.jpg">
			</div>
			<div class='cuadros scrollflow -pop -opacity'><img src="img/cuadro1.jpg">
			</div>
			<div class='cuadros scrollflow -pop -opacity'><img src="img/cuadro1.jpg">
			</div>
			<div class='cuadros scrollflow -pop -opacity'><img src="img/cuadro1.jpg">
			</div>
			<div class='cuadros scrollflow -pop -opacity'><img src="img/cuadro1.jpg">
			</div>
			<div class='cuadros scrollflow -pop -opacity'><img src="img/cuadro1.jpg">
			</div>
			<div class='cuadros scrollflow -pop -opacity'><img src="img/cuadro1.jpg">
			</div>


		</div>
	</div>
	<footer>
		<span>Tour Operator</span>
		<span>Canopy River Puerto Vallarta Boulevard Francisco Medina Ascencio #1989, Local H2-A</span>
		<span>Zona Hotelera Norte Plaza las Glorias. Tel. 222 05 60 y 222 08 60</span>
		<span>2016 Canopy River. ALL Rights Reserved. Official Website | FAQ | SITE MAP | PRIVACY POLICY | TERMS OF USE</span>
	</footer>

</body>
</html>